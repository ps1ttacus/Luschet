# Disclaimer
Mein Gott, das ist ein Spaßprojekt. Niemand soll sich angegriffen fühlen, seid lieb zueinander.   
Dieses Add-On sollte nur zum Spaß temporär bei Firefox eingebunden werden. 
Firefox wird dadurch ein bisschen langsamer, das könnte man ändern, aber überragende Performance war nicht das Ziel.

Zur Einrichtung, siehe weiter unten **Einrichtung**

# Luschet

Behebt die häufige falsche Schreibweise des Namen eines kürzlich bekannt gewordenen Politikers des Club Deutscher Unternehmer

# Einrichtung

1. Hier von Git clonen
2. Man gehe bei Firefox auf [about:debugging](about:debugging)
    - Achtung: Bei mir kam zuerst eine "Setup" Seite. Einfach links auf "This Firefox" klicken
3. Man klicke "Load temporary Add-on..." (bzw. deutsches pendant)
4. Man wähle entweder die manifest.json oder die luschet.js Datei aus. Sollte beides gehen. Zum testen habe ich immer die luschet.js Datei genommen.
5. Auf [spiegel.de](https://www.spiegel.de/) gehen und die Nachrichten genießen
